package com.example.cloud_computing_project;

import com.example.dao.Post;
import com.example.dao.UserInfo;
import com.example.service.PostService;
import com.example.service.UserInfoService;
import com.example.userinput.LoginInput;
import com.example.userinput.PassEmailInput;
import com.example.userinput.PassResetInput;
import org.apache.catalina.User;
import org.hibernate.annotations.Parameter;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Currency;
import java.util.List;

@Controller
public class PasswordResetController {

	private final String CURRENT_PAGE = "resetpassword";

    private UserInfoService userInfoService;
    private SESSender emailSender;

    @Autowired
    public PasswordResetController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
        emailSender = new SESSender();
    }

    @GetMapping("/passresetemail")
    public String displayEmailConfirmation(Model model) {
        model.addAttribute("passInput", new PassEmailInput());
        return "passresetemail";
    }

    @PostMapping("/passresetemail")
    public String postEmailConfirmation(@ModelAttribute PassEmailInput passInput, Model model) {
        model.addAttribute("passInput", passInput);
        passInput.setEvaluateFoundProfile(true);
        UserInfo userInfo = userInfoService.findProfile(passInput.getEmail());
        if (userInfo != null) {
            passInput.setUserFound(true);
            emailSender.sendPasswordReset(userInfo.getEmail());
        }
        return "passresetemail";
    }

    @GetMapping("/" + CURRENT_PAGE)
    public String displayPassResetPage(@RequestParam("email") String email, Model model) {
        model.addAttribute("passInput", new PassResetInput());
        model.addAttribute("email", email);
        return CURRENT_PAGE;
    }

    @PostMapping("/" + CURRENT_PAGE)
    public String savePassUpdate(@CookieValue(value = "email", defaultValue = "") String email, @ModelAttribute PassResetInput passInput, Model model) {
        model.addAttribute("passInput", passInput);
        passInput.setEvaluatePass(true);
        if (passInput.passwordsMatch()) {
            userInfoService.updatePassword(email, passInput.getPassword1());
        }
        //model.addAttribute("passInput", passInput);
        return CURRENT_PAGE;
    }
}
