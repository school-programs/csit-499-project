package com.example.cloud_computing_project;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest; 

public class SESSender {

  AmazonSimpleEmailService client = null;
  String toEmail = "";
  String subject = "";
  String htmlBody = "";
  String textBody = "";

  // This address must be verified with Amazon SES.
  static final String FROM = "garverbm@lopers.unk.edu";

  public SESSender() {
    client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
  }

  private void sendMessage() {
    try {
      SendEmailRequest request = new SendEmailRequest()
              .withDestination(
                      new Destination().withToAddresses(toEmail))
              .withMessage(new Message()
                      .withBody(new Body()
                              .withHtml(new Content()
                                      .withCharset("UTF-8").withData(htmlBody))
                              .withText(new Content()
                                      .withCharset("UTF-8").withData(textBody)))
                      .withSubject(new Content()
                              .withCharset("UTF-8").withData(subject)))
              .withSource(FROM);
      client.sendEmail(request);
    } catch (Exception e) {}
  }

  public void sendWelcomeEmail(String toEmail, String username) {
    this.toEmail = toEmail;
    subject = "Welcome to E-Posts";
    try {
      byte[] encoded = Files.readAllBytes(Paths.get("src",  "main", "resources", "templates", "email", "welcome.html"));
      htmlBody = new String(encoded, Charset.defaultCharset());
      htmlBody = htmlBody.replace("ABCINSERTEDUSERNAMEABC", username);
      textBody = "Welcome to E-Posts\n" +
              "E-Posts is a censor-free posting forum.\n\n" +
              "We are glad you joined " + username + "\n" +
              "Go to https://eposts.thegarver.com to get started posting on our global forum.";
    } catch (Exception e) {}
    sendMessage();
  }

  public void sendPasswordReset(String toEmail) {
    this.toEmail = toEmail;
    subject = " E-Posts Password Reset";
    try {
      byte[] encoded = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "email", "passreset.html"));
      htmlBody = new String(encoded, Charset.defaultCharset());
      htmlBody = htmlBody.replace("ABCINSERTEDEMAILABC", toEmail);
      textBody = "Password Reset\n\n" +
              "We have received a request to reset your password.\n" +
              "If this was not you, please ignore this E-Mail.\n\n" +
              "If you did request this, please go to https://eposts.thegarver.com/resetpassword?email=" + toEmail + "\n" +
              "If you have any issues, please contact garverbm@lopers.unk.edu";
    } catch (Exception e) {}
    sendMessage();
  }
}