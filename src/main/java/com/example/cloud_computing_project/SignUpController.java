package com.example.cloud_computing_project;

import com.example.dao.Post;
import com.example.dao.UserInfo;
import com.example.service.PostService;
import com.example.service.UserInfoService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class SignUpController {

    private UserInfoService userInfoService;
    private SESSender emailSender;
    private int userExists = 0;

    @Autowired
    public SignUpController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
        emailSender = new SESSender();
    }

    private final String CURRENT_PAGE = "signup";


    @GetMapping("/")
    public String displaySignUpPage(@CookieValue(value = "email", defaultValue = "") String email, Model model) {
        if (email.matches("removecookie@novalidemail.tmp") || email.isEmpty()) {
            model.addAttribute("userInfo", new UserInfo());
            model.addAttribute("userExists", userExists);
            return CURRENT_PAGE;
        }
        return "redirect:/posts";
    }

    @PostMapping("/")
    public String saveNewUser(HttpServletResponse response, @ModelAttribute UserInfo userInfo, Model model) {
        model.addAttribute("userExists", userExists);
        UserInfo foundUser = userInfoService.findProfile(userInfo.getEmail());
        if (foundUser == null){
            userInfoService.createProfile(userInfo);
            Cookie cookie = new Cookie("email", userInfo.getEmail());
            response.addCookie(cookie);
            emailSender.sendWelcomeEmail(userInfo.getEmail(), userInfo.getUser_name());
            return "redirect:/posts";
        }
        userExists = 1;
        return "redirect:/";
    }
}
