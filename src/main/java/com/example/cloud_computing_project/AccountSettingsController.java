package com.example.cloud_computing_project;

import com.example.dao.UserInfo;
import com.example.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AccountSettingsController {

    private UserInfoService userInfoService;

    @Autowired
    public AccountSettingsController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private final String CURRENT_PAGE = "acctsettings";


    @GetMapping("/" + CURRENT_PAGE)
    public String displayAcctSettingsPage(@CookieValue(value = "email", defaultValue = "") String email, Model model) {
        UserInfo userInfo = userInfoService.findProfile(email);
        if (userInfo == null) return "redirect:/";
        model.addAttribute("userInfo", userInfo);
        return CURRENT_PAGE;
    }

    @PostMapping("/" + CURRENT_PAGE)
    public String saveUpdates(@CookieValue(value = "email", defaultValue = "") String email, @ModelAttribute UserInfo userInfo, Model model) {
        UserInfo returnedProfile = userInfoService.updateProfile(userInfo, email); //This is due to spring not returning the email in model for some reason
        model.addAttribute("userInfo", returnedProfile);
        model.addAttribute("updated", 1);
        return CURRENT_PAGE;
    }
}
