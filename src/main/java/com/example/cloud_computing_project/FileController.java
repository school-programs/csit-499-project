package com.example.cloud_computing_project;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.amazonaws.util.ImmutableMapParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
@Controller
public class FileController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @PostMapping("/file")
    @ResponseBody
    public Map<String,String> imageUpload(HttpServletRequest request, @RequestParam("mediafile") MultipartFile file) {
        S3FileIntractor s3FileIntractor = new S3FileIntractor();

        try {
            File convertedFile = convertToFile(file);
            String AWSFilePath = s3FileIntractor.UploadFile(convertedFile);
            return ImmutableMapParameter.of("link", AWSFilePath);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return ImmutableMapParameter.of("error", e.getMessage());
        }

    }

    public File convertToFile(MultipartFile file) throws IOException {
        try {
            byte[] bytes = file.getBytes();

            String osBasePath = "";
            String osName = System.getProperty("os.name").toLowerCase();
            if (osName.contains("win")) {
                osBasePath = "C:\\TempServFiles";
            }else if (osName.contains("nux")) {
                osBasePath = "/home/TempServFies";
             }
            // Creating the directory to store file
            File dir = new File(osBasePath + File.separator);
            if (!dir.exists())
                dir.mkdirs();

            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()
                    + File.separator + file.getOriginalFilename());
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();

            System.out.println("Server File Location="
                    + serverFile.getAbsolutePath());

            return serverFile;
        } catch (Exception e) {
            return null;
        }
    }
}