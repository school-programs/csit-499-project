package com.example.cloud_computing_project;

import com.example.dao.Post;
import com.example.service.PostService;
import com.example.service.UserInfoService;
import com.example.userinput.LoginInput;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PostsController {

    private PostService postService;
    private UserInfoService userInfoService;

    @Autowired
    public PostsController(PostService postService, UserInfoService userInfoService) {
        this.postService = postService;
        this.userInfoService = userInfoService;
    }

    private final String CURRENT_PAGE = "posts";


    @GetMapping("/" + CURRENT_PAGE)
    public String displayPostsPage(@CookieValue(value = "email", defaultValue = "") String email, Model model) {
        if (email.isEmpty()) return "redirect:/";
        model.addAttribute("posts", postService.getAllPosts());
        return CURRENT_PAGE;
    }

    @PostMapping("/" + CURRENT_PAGE)
    public String savePost(@CookieValue(value = "email") String email, @RequestParam("htmlinput") String postHTML) {
        Post post = new Post();
        post.setPost(postHTML);
        post.setPost_time(DateTime.now().toDate());
        post.setUserInfo(userInfoService.findProfile(email));

        postService.addPost(post);
        return CURRENT_PAGE;
    }
}
