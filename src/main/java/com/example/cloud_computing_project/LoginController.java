package com.example.cloud_computing_project;

import com.example.dao.UserInfo;
import com.example.service.UserInfoService;
import com.example.userinput.LoginInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {

    private UserInfoService userInfoService;

    @Autowired
    public LoginController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private final String LOGIN_PAGE = "login";
    private final String USER_INPUT = "userInput";

    @GetMapping("/" + LOGIN_PAGE)
    public String displayLogin(Model model) {
        model.addAttribute(USER_INPUT, new LoginInput());
        return LOGIN_PAGE;
    }

    @PostMapping("/" + LOGIN_PAGE)
    public String verifyLogin(HttpServletResponse response, @ModelAttribute LoginInput userInput, Model model) {
        userInput.setPasswordValid(true);
        userInput.setEmailValid(true);
        UserInfo user = userInfoService.findProfile(userInput.getEmail());
        if (user == null) {
            userInput.setEmailValid(false);
        } else {
            if (userInfoService.passwordCorrect(user, userInput.getPassword())){
                Cookie cookie = new Cookie("email", user.getEmail());
                response.addCookie(cookie);
                return "redirect:/posts";
            } else {
                userInput.setPasswordValid(false);
            }
        }
        userInput.setPassword("");
        model.addAttribute(USER_INPUT, userInput);
        return LOGIN_PAGE;
    }
}
