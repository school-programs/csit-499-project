package com.example.userinput;

public class PassEmailInput {

    private String email = "";

    private boolean userFound = false;

    private boolean evaluateFoundProfile = false;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUserFound() {
        return userFound;
    }

    public void setUserFound(boolean userFound) {
        this.userFound = userFound;
    }

    public boolean isEvaluateFoundProfile() {
        return evaluateFoundProfile;
    }

    public void setEvaluateFoundProfile(boolean evaluateFoundProfile) {
        this.evaluateFoundProfile = evaluateFoundProfile;
    }
}
