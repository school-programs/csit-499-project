package com.example.userinput;

public class PassResetInput {

    private String password1 = "";

    private String password2 = "";

    private boolean evaluatePass = false;

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public boolean isEvaluatePass() {
        return evaluatePass;
    }

    public void setEvaluatePass(boolean evaluatePass) {
        this.evaluatePass = evaluatePass;
    }

    public boolean passwordsMatch() {
        if ((password1 != "" && password2 != "") && password1.matches(password2)) {
            return true;
        }
        return false;
    }
}
