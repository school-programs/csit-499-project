package com.example.dao;

import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "Posts")
@Access(value=AccessType.FIELD)
public class Post implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_email")
    private UserInfo userInfo;

    @Column(columnDefinition="text")
    private String post;

    @Column(columnDefinition="datetime")
    private Date post_time;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Date getPost_time() {
        return post_time;
    }

    public void setPost_time(Date post_time) {
        this.post_time = post_time;
    }

    public String getFormatedDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy h:mm a");
        return simpleDateFormat.format(post_time);
    }
}
