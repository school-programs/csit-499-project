package com.example.dao;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UserInfo")
@Access(value=AccessType.FIELD)
public class UserInfo implements Serializable {

    @Id
    @Column(columnDefinition="varchar(255)")
    private String email;

    @Column(columnDefinition="varchar(50)")
    private String user_name;

    @Column(columnDefinition="varchar(255)")
    private String first_name;

    @Column(columnDefinition="varchar(255)")
    private String last_name;

    @Column(columnDefinition="varchar(255)")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
