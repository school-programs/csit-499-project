package com.example.dao.repository;

import com.example.dao.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PostRepository extends CrudRepository<Post, String> {

    @Query
    List<Post> findAllByIdNotNullOrderByIdDesc();

}
