package com.example.service;

import com.example.dao.Post;
import com.example.dao.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PostService {

    @Autowired
    protected PostRepository postRepository;

    public List<Post> getAllPosts()
    {
        return postRepository.findAllByIdNotNullOrderByIdDesc();
    }

    public void addPost(Post post) {
        postRepository.save(post);
    }
}
