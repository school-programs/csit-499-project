package com.example.service;

import com.example.dao.UserInfo;
import com.example.dao.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

@Service
@Transactional
public class UserInfoService {

    @Autowired
    UserInfoRepository userInfoRepository;

    public UserInfo findProfile(String emailInput) {
        return userInfoRepository.findByEmail(emailInput);
    }

    public void createProfile(UserInfo userInfo) {
        userInfo.setPassword(encryptPassword(userInfo.getPassword()));
        userInfoRepository.save(userInfo);
    }

    public UserInfo updateProfile(UserInfo userInfo, String email) {
        UserInfo foundProfile = findProfile(email);
        foundProfile.setFirst_name(userInfo.getFirst_name());
        foundProfile.setLast_name(userInfo.getLast_name());
        foundProfile.setUser_name(userInfo.getUser_name());
        userInfoRepository.save(foundProfile);
        return foundProfile;
    }

    public boolean passwordCorrect(UserInfo user, String inputPassword) {
        String storedPass = user.getPassword();
        String inputPass = encryptPassword(inputPassword);
        return storedPass.matches(inputPass);
    }

    public void updatePassword(String email, String password) {
        UserInfo foundProfile = findProfile(email);
        foundProfile.setPassword(encryptPassword(password));
        userInfoRepository.save(foundProfile);
    }

    private String encryptPassword(String password) {
        /* Derive the key, given password and salt. */
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(password.getBytes(StandardCharsets.UTF_8));
            BigInteger number = new BigInteger(1, hash);
            StringBuilder hexString = new StringBuilder(number.toString(16));

            // Pad with leading zeros
            while (hexString.length() < 32)
            {
                hexString.insert(0, '0');
            }

            return hexString.toString();

        } catch (Exception e) { return "";}
    }
}
